\select@language {german}
\contentsline {section}{\numberline {1}Allgemeines Serversetup}{3}{section.1}
\contentsline {section}{\numberline {2}Server Zertifikat}{4}{section.2}
\contentsline {section}{\numberline {3}Openfire}{4}{section.3}
\contentsline {subsection}{\numberline {3.1}Installation des Pakets}{4}{subsection.3.1}
\contentsline {subsection}{\numberline {3.2}Einrichten von Openfire}{4}{subsection.3.2}
\contentsline {subsection}{\numberline {3.3}Einrichtung der Datenbank f\IeC {\"u}r Openfire}{8}{subsection.3.3}
\contentsline {subsection}{\numberline {3.4}Zertifikat in Openfire}{8}{subsection.3.4}
\contentsline {section}{\numberline {4}E-Mail}{9}{section.4}
\contentsline {subsection}{\numberline {4.1}Server}{9}{subsection.4.1}
\contentsline {subsection}{\numberline {4.2}Userverwaltung}{9}{subsection.4.2}
\contentsline {subsection}{\numberline {4.3}Webfrontend}{9}{subsection.4.3}
\contentsline {subsection}{\numberline {4.4}Spamfilter und AntiVirus}{9}{subsection.4.4}
\contentsline {subsection}{\numberline {4.5}Loginanalyzer}{9}{subsection.4.5}
\contentsline {section}{\numberline {5}Webserver}{9}{section.5}
\contentsline {section}{\numberline {6}CloudStorage}{9}{section.6}
\contentsline {subsection}{\numberline {6.1}OwnCloud}{9}{subsection.6.1}
\contentsline {subsection}{\numberline {6.2}Installation}{9}{subsection.6.2}
\contentsline {subsection}{\numberline {6.3}Einrichtung}{9}{subsection.6.3}
\contentsline {section}{\numberline {7}Kommunikation}{9}{section.7}
\contentsline {subsection}{\numberline {7.1}Installation}{9}{subsection.7.1}
\contentsline {section}{\numberline {8}DevBasis}{10}{section.8}
\contentsline {subsection}{\numberline {8.1}Java}{11}{subsection.8.1}
\contentsline {subsection}{\numberline {8.2}JCE-8}{11}{subsection.8.2}
\contentsline {section}{\numberline {9}Security}{11}{section.9}
