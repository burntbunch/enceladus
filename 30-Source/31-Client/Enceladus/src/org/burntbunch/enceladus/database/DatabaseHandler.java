package org.burntbunch.enceladus.database;

import android.database.sqlite.SQLiteOpenHelper;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.content.ContentValues;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;

import org.burntbunch.enceladus.model.Contact;
import org.burntbunch.enceladus.model.Conversation;
import org.burntbunch.enceladus.model.EnceladusMessage;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;

/**
 * Copyright 2014 BurntBunch-Team (www.burntbunch.org)

 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at

 http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 */

public class DatabaseHandler extends SQLiteOpenHelper {

    // All Static variables
    // Database Version
    private static final int DATABASE_VERSION = 2;

    // Table names
    private static final String TABLE_CONTACTS = "contact";
    private static final String TABLE_MESSAGES = "message";

    // Contacts table column names
    private static final String KEY_PARTNER_JID = "partner_jid";
    private static final String KEY_PARTNER = "partner";
    private static final String KEY_AVATAR = "avatar";
    
    // Message table column names
    private static final String KEY_MESSAGE_ID = "msg_id";
    private static final String KEY_MESSAGE = "msg";
    private static final String KEY_TIME = "time";
    private static final String KEY_SENT_READ = "sent"; //null = none, 0 = sent, 1 = read
    private static final String KEY_SENT_BY = "sent_by"; // 0 = TYPE_ME, 1 = TYPE_YOU
    
    public interface DatabaseListener {
    	public void onDatabaseCreated();
    }

    public DatabaseHandler(Context context, String name) {
        super(context, name, null, DATABASE_VERSION);
    }

    // Creating Tables

    @Override
    public void onCreate(SQLiteDatabase db) {
    	
        String CREATE_TABLE_CONTACTS = "CREATE TABLE " + TABLE_CONTACTS + "("
        									  + KEY_PARTNER_JID + " TEXT PRIMARY KEY, "
        									  + KEY_PARTNER + " TEXT, "
                                              + KEY_AVATAR + " BLOB"
        									  + ")";

        String CREATE_TABLE_MESSAGES = "CREATE TABLE " + TABLE_MESSAGES + "("
                                                      + KEY_MESSAGE_ID + " TEXT PRIMARY KEY, "
                                                      + KEY_PARTNER_JID + " INTEGER NOT NULL, "
                                                      + KEY_MESSAGE + " TEXT NOT NULL, "
                                                      + KEY_TIME + " INTEGER NOT NULL, "
                                                      + KEY_SENT_READ + " INTEGER, "
                                                      + KEY_SENT_BY + " INTEGER, "
                                                      + "FOREIGN KEY(" + KEY_PARTNER_JID + ") "
                                                      + "REFERENCES " + TABLE_CONTACTS + "(" + KEY_PARTNER_JID + ")"
                                                      + ")";

        db.execSQL(CREATE_TABLE_CONTACTS);
        db.execSQL(CREATE_TABLE_MESSAGES);
    }
    
    @Override
    public void onOpen(SQLiteDatabase db) {
    	super.onOpen(db);
    }
    
    // Adding new Contact
    public void addContact(Contact con) {
        SQLiteDatabase db = this.getWritableDatabase();

        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        con.getAvatar().compress(Bitmap.CompressFormat.PNG, 100, stream);

        //Conversation Values
        ContentValues val = new ContentValues();
        val.put(KEY_PARTNER_JID, con.getjID());
        val.put(KEY_PARTNER, con.getName());
        val.put(KEY_AVATAR, stream.toByteArray());

        try {
            stream.close();
        } catch (IOException e) {
            Log.v("Database", "IO Error");
        }

        // Inserting Row
        db.insertWithOnConflict(TABLE_CONTACTS, null, val, SQLiteDatabase.CONFLICT_REPLACE);
        db.close(); // Closing database connection
    }

    // Adding new Message
    public void addMessage(EnceladusMessage msg) {
        SQLiteDatabase db = this.getWritableDatabase();

        //Conversation Values
        ContentValues val = new ContentValues();

        val.put(KEY_MESSAGE_ID, msg.getPacketID());
        if(msg.getFrom() == null){
            val.put(KEY_PARTNER_JID, msg.getTo());
            val.put(KEY_SENT_BY, 0);
        } else {
            val.put(KEY_PARTNER_JID, msg.getFrom());
            val.put(KEY_SENT_BY, 1);
        }
        val.put(KEY_MESSAGE, msg.getBody());
        val.put(KEY_TIME, msg.getDate().getTime());
        if(msg.isReceived()){
            val.put(KEY_SENT_READ, 1);
        } else if (msg.isSent()){
            val.put(KEY_SENT_READ, 0);
        }

        // Inserting Row
        db.insertWithOnConflict(TABLE_MESSAGES, null, val, SQLiteDatabase.CONFLICT_REPLACE);
        db.close(); // Closing database connection
    }

    // Getting all contacts
    public ArrayList<Contact> getContacts() {

        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.query(TABLE_CONTACTS, new String[] { KEY_PARTNER_JID,KEY_PARTNER, KEY_AVATAR}, null, null, null, null, null, null);

        ArrayList<Contact> contacts = new ArrayList<Contact>();

        if (cursor != null){
            cursor.move(-1);
            while (cursor.moveToNext()){

                byte[] avatar = cursor.getBlob(cursor.getColumnIndex(KEY_AVATAR));

                Contact con = new Contact(cursor.getString(cursor.getColumnIndex(KEY_PARTNER_JID)),
                                          BitmapFactory.decodeByteArray(avatar, 0, avatar.length),
                                          cursor.getString(cursor.getColumnIndex(KEY_PARTNER)));
                contacts.add(con);
            }
        }

        return contacts;
    }

    // Getting all conversations
    public ArrayList<Conversation> getConversations(ArrayList<Contact> contacts) {

        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.query(TABLE_MESSAGES, new String[]{KEY_MESSAGE_ID, KEY_PARTNER_JID, KEY_MESSAGE, KEY_TIME, KEY_SENT_READ, KEY_SENT_BY}, null, null, null, null, KEY_TIME, null);

        ArrayList<Conversation> conversations = new ArrayList<Conversation>();
        ArrayList<String> jIDs = new ArrayList<String>();

        if (cursor != null){
            cursor.move(-1);
            while (cursor.moveToNext()){

                int sentBy = cursor.getInt(cursor.getColumnIndex(KEY_SENT_BY));
                String partner = cursor.getString(cursor.getColumnIndex(KEY_PARTNER_JID));

                EnceladusMessage msg = new EnceladusMessage(
                        cursor.getString(cursor.getColumnIndex(KEY_MESSAGE_ID)),
                        partner,
                        cursor.getString(cursor.getColumnIndex(KEY_MESSAGE)),
                        cursor.getLong(cursor.getColumnIndex(KEY_TIME)),
                        cursor.getInt(cursor.getColumnIndex(KEY_SENT_READ)),
                        sentBy
                );

                if(jIDs.contains(partner)){
                    conversations.get(jIDs.indexOf(cursor.getString(cursor.getColumnIndex(KEY_PARTNER_JID)))).addMessage(msg, true);
                } else {
                    Conversation con = new Conversation();
                    for (Contact c : contacts) {
                        if(c.getjID().equals( (sentBy == 1) ? msg.getFrom() : msg.getTo())){
                            con.setPartner(c);
                            jIDs.add(c.getjID());
                        }
                    }
                    con.addMessage(msg, true);
                    conversations.add(con);
                }
                //conversations.add(null);
            }
        }
        return conversations;
    }


    // Getting All Conversations
    //public List<Conversation> getAllConversations() {
        //TODO:
   // }

    // Getting Conversation Count
   // public int getConversationCount() {
        //TODO:
   // }

    // Updating single Conversation
   // public int updateContact(Conversation con) {
        //TODO:
   // }

    // Deleting single Conversation
    public void deleteConversation(Conversation con) {
        //TODO:
    }

    // Upgrading database
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // Drop older table if existed
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_CONTACTS);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_MESSAGES);

        // Create tables again
        onCreate(db);
    }

}
