package org.burntbunch.enceladus.model;

import org.jivesoftware.smack.packet.Message;

import java.util.Date;

/**
 * Copyright 2014 BurntBunch-Team (www.burntbunch.org)

 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at

 http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 */

public class EnceladusMessage extends Message {

    private boolean sent, received;
    private Date date;

    public EnceladusMessage(String to, Type type){
        super(to,type);
        setDate(new Date());
    }

    public EnceladusMessage(String packetID, String partner, String body, long date, int sentRead, int sentBy){
        this.setPacketID(packetID);
        this.setBody(body);
        this.setDate(new Date(date));
        if(sentBy == 1){
            this.setFrom(partner);
        } else {
            this.setTo(partner);
        }
        switch (sentRead){
            case(0):
                setSent(true);
                setReceived(false);
                break;
            case (1):
                setSent(true);
                setReceived(true);
                break;
            default:
                setSent(false);
                setReceived(false);
        }
    }

    public EnceladusMessage(Message msg){
        this.setBody(msg.getBody());
        this.setError(msg.getError());
        this.setFrom(msg.getFrom());
        this.setLanguage(msg.getLanguage());
        this.setPacketID(msg.getPacketID());
        this.setSubject(msg.getSubject());
        this.setThread(msg.getThread());
        this.setTo(msg.getTo());
        this.setType(msg.getType());
        setDate(new Date());
    }

    public boolean isSent() {
        return sent;
    }

    public void setSent(boolean sent) {
        this.sent = sent;
    }

    public boolean isReceived() {
        return received;
    }

    public void setReceived(boolean received) {
        this.received = received;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }


}
