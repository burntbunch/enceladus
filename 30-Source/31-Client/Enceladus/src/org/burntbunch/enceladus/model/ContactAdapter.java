package org.burntbunch.enceladus.model;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import org.burntbunch.enceladus.R;
import org.jivesoftware.smack.SmackException.NoResponseException;
import org.jivesoftware.smack.SmackException.NotConnectedException;
import org.jivesoftware.smack.XMPPException.XMPPErrorException;

import java.text.SimpleDateFormat;
import java.util.List;

/**
 * Copyright 2014 BurntBunch-Team (www.burntbunch.org)

 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at

 http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 */

public class ContactAdapter extends ArrayAdapter<Contact>{

    private List<Contact> contacts;
    private Context context;

    public ContactAdapter(Context context, int resID,  List<Contact> contacts){
        super(context, resID, contacts);
        this.context = context;
        this.contacts = contacts;
    }

    public void setContacts(List<Contact> con){
        this.contacts = con;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row = convertView;
        ContactView cw;

        if (row == null) {
            LayoutInflater inflater =(LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            row = inflater.inflate(R.layout.contact_list_row, parent, false);

            cw = new ContactView();
            cw.avatar = (ImageView) row.findViewById(R.id.contact_avatar);
            cw.name = (TextView) row.findViewById(R.id.contact_name);
            cw.date = (TextView)row.findViewById(R.id.contact_lastSeen);
            cw.jID = (TextView)row.findViewById(R.id.contact_jID);
            row.setTag(cw);
        }else{
            cw = (ContactView)row.getTag();
        }

        Contact con = contacts.get(position);

        cw.name.setText(con.getName());
        try {
        	if (con.getLastSeen() != null)
        		cw.date.setText(getContext().getText(R.string.last_seen) + " "
        						+ SimpleDateFormat.getDateTimeInstance().format(con.getLastSeen()));
        	else
        		cw.date.setText("");
		} catch (NoResponseException | XMPPErrorException | NotConnectedException e) {
			cw.date.setText("");
		}

        cw.avatar.setImageBitmap(con.getAvatar());

        cw.jID.setText(con.getjID());
        return row;
    }

    protected static class ContactView{
        protected TextView name;
        protected TextView date;
        protected ImageView avatar;
        protected TextView jID;
    }
}
