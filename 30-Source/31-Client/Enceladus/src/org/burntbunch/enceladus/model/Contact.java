package org.burntbunch.enceladus.model;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import java.util.Date;

import org.burntbunch.enceladus.R;
import org.jivesoftware.smack.SmackException.NoResponseException;
import org.jivesoftware.smack.SmackException.NotConnectedException;
import org.jivesoftware.smack.XMPPException.XMPPErrorException;
import org.jivesoftware.smack.util.StringUtils;
import org.jivesoftware.smackx.iqlast.LastActivityManager;

/**
 * Copyright 2014 BurntBunch-Team (www.burntbunch.org)

 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at

 http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 */

public class Contact {

    private Bitmap avatar;
    private String visibleName;
    private String jID;
    private static LastActivityManager lastActivity;


    private static Resources resources;

    public Contact(String jID){
        this(jID, null, StringUtils.parseName(jID));
    }

    public Contact(String jID, Bitmap res, String visibleName) {
        this.jID = jID;
        this.visibleName = visibleName;
        if (res == null) {
            this.avatar = BitmapFactory.decodeResource(resources, R.drawable.logo_flat);
        } else {
            this.avatar = res;
        }
    }
    
    public Bitmap getAvatar() {
        return avatar;
    }

    public void setAvatar(Bitmap avatar) {
        if(avatar != null)
            this.avatar = avatar;
        else
            this.avatar = BitmapFactory.decodeResource(resources, R.drawable.logo_flat);
    }

    public Date getLastSeen() throws NoResponseException, XMPPErrorException, NotConnectedException {
    	if (lastActivity != null) {
    		return new Date(new Date().getTime() - lastActivity.getLastActivity(jID).lastActivity * 1000);
    	} else {
    		return null;
    	}
        
    }
    
    public static void setLastActivityManager(LastActivityManager la) {
    	lastActivity = la;
    }

    public String getName() {
        return visibleName;
    }

    public void setName(String name) {
        this.visibleName = name;
    }

    public String getjID() {
        return jID;
    }

    public void setjID(String jID) {
        this.jID = jID;
    }

    public static void setResources(Resources resources) {
        Contact.resources = resources;
    }

}
