package org.burntbunch.enceladus.model;

import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import org.burntbunch.enceladus.R;
import org.jivesoftware.smack.packet.Message;

/**
 * Copyright 2014 BurntBunch-Team (www.burntbunch.org)

 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at

 http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 */

public class UserListAdapter extends ArrayAdapter<Conversation> {

	private final List<Conversation> values;
	private Context context;
	
	private static class ConversationHolder {
		TextView partner, lastMessage;
        ImageView avatar;
	}
	
	public UserListAdapter(Context context, List<Conversation> values) {
		super(context, R.layout.user_list_row , values);
		this.context = context;
		this.values = values;
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View row = convertView;
		ConversationHolder holder;
		if (row == null) {
			LayoutInflater inflater = 
					(LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);	
			row = inflater.inflate(R.layout.user_list_row, parent, false);
			holder = new ConversationHolder();
			holder.partner = (TextView)row.findViewById(R.id.user_list_headline);
			holder.lastMessage = (TextView)row.findViewById(R.id.user_list_description);
            holder.avatar = (ImageView)row.findViewById(R.id.user_list_icon);
			row.setTag(holder);
		}
		else 
			holder = (ConversationHolder)row.getTag();
		holder.partner.setText(values.get(position).getPartner().getName());
        Message m = values.get(position).getLastMessage();
        holder.avatar.setImageBitmap(values.get(position).getPartner().getAvatar());
        if(m != null)
            holder.lastMessage.setText(m.getBody());
        else
		    holder.lastMessage.setText("N/A");
		return row;
	}
	

}
