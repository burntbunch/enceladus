package org.burntbunch.enceladus.model;

import java.text.SimpleDateFormat;
import java.util.ArrayList;

import org.burntbunch.enceladus.R;
import org.jivesoftware.smack.packet.Message;

import android.content.Context;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TableRow;
import android.widget.TextView;

/**
 * Copyright 2014 BurntBunch-Team (www.burntbunch.org)

 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at

 http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 */

public class ChatAdapter extends BaseAdapter {
	
	public static final int TYPE_ME = 0;
    public static final int TYPE_YOU = 1;
    public static final int TYPE_MAX_COUNT = 2;
	private ArrayList<EnceladusMessage> messages;
	private LayoutInflater inflater;

	public ChatAdapter(Context context) {
		inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        messages = new ArrayList<EnceladusMessage>();
	}

    public void reloadConversation(ArrayList<EnceladusMessage> conversation){
        this.messages = conversation;
        notifyDataSetChanged();
    }

	 @Override
     public int getCount() {
         return messages.size();
     }

     @Override
     public Message getItem(int position) {
         return messages.get(position);
     }

     @Override
     public long getItemId(int position) {
         return position;
     }
     
     @Override
     public int getItemViewType(int position) {
         return messages.get(position).getFrom() == null ? TYPE_ME : TYPE_YOU;
     }

     @Override
     public int getViewTypeCount() {
         return TYPE_MAX_COUNT;
     }

     @Override
     public View getView(int position, View convertView, ViewGroup parent) {
         ViewHolder holder;
         if (convertView == null) {
        	 holder = new ViewHolder();
             convertView = inflater.inflate(R.layout.chat_row, null);
             holder.message = (TextView)convertView.findViewById(R.id.message_text);
             holder.date = (TextView)convertView.findViewById(R.id.message_time);
             holder.received = (ImageView)convertView.findViewById(R.id.message_received);
             convertView.setTag(holder);
         } else {
             holder = (ViewHolder)convertView.getTag();
         }
         holder.message.setText(messages.get(position).getBody());
         holder.date.setText(SimpleDateFormat.getTimeInstance().format(messages.get(position).getDate()));
         if(getItemViewType(position) == TYPE_ME) {
        	 ((TableRow)convertView.findViewById(R.id.row_layout)).setGravity(Gravity.RIGHT);
        	 (convertView.findViewById(R.id.speech_bubble)).setBackgroundResource(R.drawable.me);
             if (messages.get(position).isReceived()) {
                 holder.received.setVisibility(View.VISIBLE);
             } else {
                 holder.received.setVisibility(View.INVISIBLE);
             }
         }
         return convertView;
     }

	 public static class ViewHolder {
	     public TextView message;
         public TextView date;
         public ImageView received;
	 }
}

