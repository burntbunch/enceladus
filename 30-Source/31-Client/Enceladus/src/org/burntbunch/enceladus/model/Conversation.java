package org.burntbunch.enceladus.model;

import org.jivesoftware.smack.Chat;
import org.jivesoftware.smack.packet.Message;

import java.util.ArrayList;

/**
 * Copyright 2014 BurntBunch-Team (www.burntbunch.org)

 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at

 http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 */

public class Conversation {
	private Contact partner;
	private ArrayList<EnceladusMessage> messages;
    private Chat chat;

    private static ArrayList<EnceladusMessage> unsentMessages = new ArrayList<EnceladusMessage>();
    private static ArrayList<EnceladusMessage> unreceivedMessages = new ArrayList<EnceladusMessage>();
	private static ArrayList<EnceladusMessage> newMessages = new ArrayList<EnceladusMessage>();
	
	public Conversation() {
		messages = new ArrayList<EnceladusMessage>();
	}

	public Contact getPartner() {
		return partner;
	}

	public void setPartner(Contact partner) {
		this.partner = partner;
	}

	public Message getLastMessage() {
        if(!messages.isEmpty())
		    return messages.get(messages.size()-1);
        else
            return null;
	}
	public ArrayList<EnceladusMessage> getMessages() {
		return messages;
	}

    public void addMessage(EnceladusMessage msg, boolean fromDatabase) {
    	if (fromDatabase && !msg.isSent()) {
    		unsentMessages.add(msg);
	    }
        if (!msg.isReceived()) {
            unreceivedMessages.add(msg);
        }
    	
        messages.add(msg);
    }

    public Chat getChat() {
        return chat;
    }

    public void setChat(Chat chat) {
        this.chat = chat;
    }

    public static ArrayList<EnceladusMessage> getUnsentMessages() {
        return unsentMessages;
    }

    public static ArrayList<EnceladusMessage> getUnreceivedMessages() {
        return unreceivedMessages;
    }

	public static ArrayList<EnceladusMessage> getNewMessages() {
		return newMessages;
	}
}
