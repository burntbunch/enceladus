package org.burntbunch.enceladus.controller;

import java.util.ArrayList;

import org.burntbunch.enceladus.R;
import org.burntbunch.enceladus.database.DatabaseHandler;
import org.burntbunch.enceladus.model.Contact;
import org.burntbunch.enceladus.model.Conversation;
import org.burntbunch.enceladus.model.EnceladusMessage;

import android.app.Notification;
import android.app.Service;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Binder;
import android.os.IBinder;
import android.util.Log;

/**
 * Copyright 2014 BurntBunch-Team (www.burntbunch.org)

 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at

 http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 */

public class DatabaseService extends Service {
	
	private final IBinder mBinder = new DatabaseBinder();
	private DatabaseHandler dbHandler;
	private ArrayList<Contact> contacts;
	private ArrayList<EnceladusMessage> messages;
    private boolean isStarted;
	
	private class ReadContactTask extends AsyncTask<Void, Void, ArrayList<Contact>> {
		@Override
		protected ArrayList<Contact> doInBackground(Void... params) {
			return dbHandler.getContacts();
		}
	
	}
	
	private class ReadConversationTask extends AsyncTask<Void, Void, ArrayList<Conversation>> {
		@Override
		protected ArrayList<Conversation> doInBackground(Void... params) {
			return dbHandler.getConversations(contacts);
		}
	
	}
	
	public void setAll(ArrayList<Contact> contacts, ArrayList<EnceladusMessage> messages) {
		this.contacts = contacts;
		this.messages = messages;
	}
	
	public ArrayList<Contact> getContacts() {
		try {
			return new ReadContactTask().execute((Void)null).get();
		} catch (Exception e) {
			Log.e("DatabaseService", "Error while reading Contacts");
		} 
		return null;
	}
	
	public ArrayList<Conversation> getConversations() {
		try {
			return new ReadConversationTask().execute((Void)null).get();
		} catch (Exception e) {
			Log.e("DatabaseService", "Error while reading Conversations");
		} 
		return null;
	}
	
	@Override
	public void onCreate() {
		super.onCreate();
        dbHandler = new DatabaseHandler(getApplicationContext(), "DaBa");
		Log.v("DatabaseService", "DatabaseService created");
	}
	
	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {

        if(isStarted()){
            return START_NOT_STICKY;
        }

        isStarted = true;

		Log.v("DatabaseService", "DatabaseService started");
		Notification n = new Notification.Builder(getApplicationContext())
				.setContentTitle("Saving to database")
				.setSmallIcon(R.drawable.notification_small)
				.build();
		startForeground(1337, n);
		new Thread(new Runnable() {
			
			@Override
			public void run() {
				for (Contact c : contacts) {
					dbHandler.addContact(c);
				}
				
				for (EnceladusMessage e : messages) {
					dbHandler.addMessage(e);
				}
                messages.clear();
				dbHandler.close();
                isStarted = false;
				DatabaseService.this.stopForeground(true);
				
			}
		}).start();
		return START_NOT_STICKY;
	}
	
	@Override
	public void onTaskRemoved(Intent rootIntent) {
		Log.v("DatabaseService", "Task removed");
		super.onTaskRemoved(rootIntent);
	}

	@Override
	public IBinder onBind(Intent arg0) {
		return mBinder;
	}
	
    public class DatabaseBinder extends Binder{
        public DatabaseService getService() {
            return DatabaseService.this;
        }
    }

    public synchronized boolean isStarted() {
        return isStarted;
    }

}
