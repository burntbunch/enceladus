package org.burntbunch.enceladus.controller;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.media.RingtoneManager;
import android.os.Binder;
import android.os.IBinder;
import android.util.Log;

import org.burntbunch.enceladus.R;
import org.burntbunch.enceladus.controller.DatabaseService.DatabaseBinder;
import org.burntbunch.enceladus.model.Contact;
import org.burntbunch.enceladus.model.Conversation;
import org.burntbunch.enceladus.model.EnceladusMessage;
import org.burntbunch.enceladus.ui.ChatActivity;
import org.jivesoftware.smack.Chat;
import org.jivesoftware.smack.ChatManager;
import org.jivesoftware.smack.ChatManagerListener;
import org.jivesoftware.smack.ConnectionConfiguration;
import org.jivesoftware.smack.MessageListener;
import org.jivesoftware.smack.Roster;
import org.jivesoftware.smack.RosterEntry;
import org.jivesoftware.smack.RosterListener;
import org.jivesoftware.smack.SmackAndroid;
import org.jivesoftware.smack.SmackException;
import org.jivesoftware.smack.SmackException.NotConnectedException;
import org.jivesoftware.smack.XMPPException;
import org.jivesoftware.smack.packet.Message;
import org.jivesoftware.smack.packet.Presence;
import org.jivesoftware.smack.tcp.XMPPTCPConnection;
import org.jivesoftware.smack.util.StringUtils;
import org.jivesoftware.smackx.iqlast.LastActivityManager;
import org.jivesoftware.smackx.ping.PingManager;
import org.jivesoftware.smackx.receipts.DeliveryReceiptManager;
import org.jivesoftware.smackx.receipts.ReceiptReceivedListener;
import org.jivesoftware.smackx.search.ReportedData;
import org.jivesoftware.smackx.search.UserSearchManager;
import org.jivesoftware.smackx.vcardtemp.packet.VCard;
import org.jivesoftware.smackx.xdata.Form;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.RSAPrivateKeySpec;
import java.security.spec.RSAPublicKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Scanner;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.locks.ReentrantLock;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocketFactory;

/**
 * Copyright 2014 BurntBunch-Team (www.burntbunch.org)

 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at

 http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 */


public class SessionService extends Service {

    public static final String SHARED_PREF_FILENAME = "Enceladus";
    public static final String NUMBER_TAG = "org.burntbunch.enceladus.JID";
    public static final String PASSWORD_TAG = "org.burntbunch.enceladus.PWD";
    public static final String PRIV_KEY_TAG = "org.burntbunch.enceladus.PrivKey";
    public static final String PUB_KEY_TAG = "org.burntbunch.org.PubKey";
    public static final String HOST = "burntbunch.org";
    public static final int PORT = 5223;
    public static final String SERVICE = "burntbunch.org";

    private int indexToNotify;
    private int countUnread;
    private boolean hasAccount;
    private boolean isStarted;
    private boolean hasKey;
    private ChatActivity chatActivity;
    private DatabaseService databaseService;
    private ConnectionConfiguration config;
    private XMPPTCPConnection connection;

    private Presence presence;
    private Roster roster;

    private Collection<RosterEntry> rosterEntries;

    private ChatManager chatManager;
    private UserSearchManager search;
    private DeliveryReceiptManager deliveryReceiptManager;

    private ExecutorService threadpool;
    private SmackAndroid asmack;
    private ArrayList<Contact> contacts;
    private ArrayList<Conversation> conversations;
    private ReentrantLock lock;
    private SharedPreferences appData;
    private Timer saveTimer;
    private ResendWorker resendWorker;
    private Thread resendWorkerThread;
    private PingManager pingManager;

    private static String userJID;
    private String pwd;
    private String savedPrivateKey;
    private String pubKey;
    private PrivateKey privateKey;

    private final IBinder mBinder = new SessionBinder();
    
    private ServiceConnection databaseServiceConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
        	databaseService = ((DatabaseBinder)service).getService();
            databaseService.setAll(getContacts(), Conversation.getNewMessages());
            contacts.addAll(databaseService.getContacts());
            conversations.addAll(databaseService.getConversations());
        }

		@Override
		public void onServiceDisconnected(ComponentName arg0) {
			// TODO Auto-generated method stub
			
		}
    };

    /* Runnable to create a Notification. */
    private Runnable alterNotification = new Runnable() {
        @Override
        public void run() {
            if(!threadpool.isShutdown()) {
                lock.lock();
                Conversation c = conversations.get(indexToNotify);
                lock.unlock();

                if(indexToNotify < 0) {
                    Log.v("Notification", "Index of Conversation to notify invalid, something went wrong!");
                    return;
                }

                if(countUnread <= 0) {
                    Log.v("Notification", "Amount of Conversations to notify invalid, something went wrong!");
                    return;
                }

                Intent intent = new Intent(getApplicationContext(), ChatActivity.class);
                intent.setAction("ChatActivity");
                intent.putExtra(ChatActivity.ACTIVE_CONVERSATION, indexToNotify);
                intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                PendingIntent pendingIntent = PendingIntent.getActivity(getApplicationContext(), 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
                Bitmap largeIcon = BitmapFactory.decodeResource(getResources(), R.drawable.ic_launcher);
                String nameToNotify = c.getPartner().getName() + ": ";
                Notification.Builder builder = new Notification.Builder(getApplicationContext())
                        .setSmallIcon(R.drawable.notification_small)
                        .setAutoCancel(true)
                        .setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION))
                        .setVibrate(new long[]{100, 100, 100, 200})
                        .setLights(Color.BLUE, 1000, 3700)
                        .setContentIntent(pendingIntent);
                if(countUnread == 1) {
                    builder.setContentTitle(c.getPartner().getName())
                            .setContentText(c.getLastMessage().getBody())
                            .setTicker(c.getLastMessage().getBody())
                            .setLargeIcon(c.getPartner().getAvatar());
                }
                else {
                    String numOfMessages = getString(R.string.multi_notification_first_half) + " "
                            + countUnread + " " + getString(R.string.multi_notification_second_half);
                    builder.setContentTitle(numOfMessages)
                            .setContentText(nameToNotify + c.getLastMessage().getBody())
                            .setTicker(nameToNotify + c.getLastMessage().getBody())
                            .setLargeIcon(largeIcon);

                }

                Notification notification = builder.build();

                NotificationManager manager = (NotificationManager)getSystemService(NOTIFICATION_SERVICE);
                manager.notify(0, notification);
            }
        }
    };

    private MessageListener defaultMessageListener = new MessageListener() {

        @Override
        public void processMessage(Chat chat, final Message message) {
            if(message.getBody() == null || message.getBody().isEmpty() || message.getBody().trim().isEmpty()) {
                return;
            }
            lock.lock();

            EnceladusMessage enceladusMessage = null;
            if(privateKey != null){
                String decrypted_message = "";
                byte[] byteMessage = hex2Bytes(message.getBody());
                ByteArrayInputStream bais = new ByteArrayInputStream(decrypt(byteMessage,privateKey));
                Scanner scanner = new Scanner(bais, "UTF-8");
                while(scanner.hasNext()) {
                    decrypted_message += scanner.next() + " ";
                }
                /*for(byte e : decrypt(byteMessage,privateKey)){
                    decrypted_message += ((char)e);
                }*/
                enceladusMessage = new EnceladusMessage(message.getTo(), message.getType());
                enceladusMessage.setPacketID(message.getPacketID());
                enceladusMessage.setBody(decrypted_message);
            }

            /* Remove the resource */
            String participant = StringUtils.parseBareAddress(chat.getParticipant());
            enceladusMessage.setFrom(participant);
            Conversation.getNewMessages().add(enceladusMessage);

            /* Search for existing conversation in the list. */
            Conversation conversation = null;

            for(Conversation c : conversations) {
                if(c.getPartner().getjID().equals(participant)) {
                    conversation = c;
                    indexToNotify = conversations.indexOf(c);
                    break;
                }
            }


            /* If not found, create it. */
            if(conversation == null) {
            	boolean found = false;
                conversation = new Conversation();
                conversation.setChat(chat);
                for (Contact con : getContacts()) {
                	if (con.getjID().equals(participant)) {
                        conversation.setPartner(con);
                        conversations.add(conversation);
                        indexToNotify = conversations.size() - 1;
                        found = true;
                	}
                }
            
	            if(!found) {
	                Contact con = new Contact(participant);
	                conversation.setPartner(con);
                    contacts.add(con);
	                conversations.add(conversation);
	                indexToNotify = conversations.size() - 1;
	            }
                
            }

            if(chatActivity != null){
                chatActivity.notifyConversationChanged();
            }
            countUnread++;
            conversation.setChat(chat);
            conversation.addMessage(enceladusMessage, false);
            lock.unlock();
            if(chatActivity == null || chatActivity.isInBackground()) {
                threadpool.execute(alterNotification);
            }

        }
    };

    public static byte[] encrypt(byte[] input, Key k){

        Cipher c;
        try {
            c = Cipher.getInstance("RSA/ECB/PKCS1Padding");
            c.init(Cipher.ENCRYPT_MODE, k);
            return c.doFinal(input);
        } catch (NoSuchAlgorithmException | NoSuchPaddingException | InvalidKeyException | IllegalBlockSizeException | BadPaddingException e) {
            //System.out.println("Bloody Hell");
            e.printStackTrace();
        }
        return null;
    }

    public static byte[] decrypt(byte[] input, Key k) {
        Cipher c;
        try {
            c = Cipher.getInstance("RSA/ECB/PKCS1Padding");
            c.init(Cipher.DECRYPT_MODE, k);
            return c.doFinal(input);
        } catch (NoSuchAlgorithmException | NoSuchPaddingException | InvalidKeyException | IllegalBlockSizeException | BadPaddingException e) {
            //System.out.println("Bloody Hell");
            e.printStackTrace();
        }
        return null;
    }

    public static byte[] hex2Bytes(String s) {
        int len = s.length();
        byte[] data = new byte[len / 2];
        for (int i = 0; i < len; i += 2) {
            data[i / 2] = (byte) ((Character.digit(s.charAt(i), 16) << 4)
                    + Character.digit(s.charAt(i+1), 16));
        }
        return data;
    }

    public static String bytes2Hex(byte[] input) {

        String output = "";
        if(input.length == 0)
            return output;
        String s;
        for(int i = 0; i < input.length; i++) {
            s = String.format("%x", input[i]);
            if(s.length() == 1)
                s = "0" + s;
            output += s;
        }
        return output;
    }

    private ReceiptReceivedListener receiptReceivedListener = new ReceiptReceivedListener() {
        @Override
        public void onReceiptReceived(String fromJid, String toJid, String receiptId) {
            EnceladusMessage foundMessage = null;
            for (EnceladusMessage e : Conversation.getUnreceivedMessages()) {
                if(e.getPacketID().equals(receiptId)) {
                    foundMessage = e;
                    break;
                }
            }
            if(foundMessage != null) {
                foundMessage.setReceived(true);
                Conversation.getUnreceivedMessages().remove(foundMessage);
                if(chatActivity != null) {
                    chatActivity.notifyConversationChanged();
                }
            }
        }
    };

    public void sendMessage(Conversation con, String message) {
            Chat chat = con.getChat();
            if(!message.isEmpty() && !message.trim().isEmpty()) {
                final EnceladusMessage msg = new EnceladusMessage(chat.getParticipant(), Message.Type.chat);
                Conversation.getNewMessages().add(msg);
                msg.setBody(message);
                con.addMessage(msg, false);
                engage(msg, con.getChat());
            }
    }

    void engage(EnceladusMessage msg, Chat chat) {

        PublicKey pub = null;
        try {
            VCard vc = new VCard();
            vc.load(connection, StringUtils.parseBareAddress(msg.getTo()));
            pubKey = vc.getField("PubKey");
            if(pubKey == null){
                Log.v("GetPublicKey", "Error while getting the senders PublicKey");
                return;
            }
            KeyFactory keyFactory = KeyFactory.getInstance("RSA");
            PublicKey publicKey =
                    keyFactory.generatePublic(new X509EncodedKeySpec(hex2Bytes(pubKey)));
            RSAPublicKeySpec rsaPublicKeySpec = keyFactory.getKeySpec(publicKey, RSAPublicKeySpec.class);

            pub = KeyFactory.getInstance("RSA").generatePublic(rsaPublicKeySpec);
        } catch (InvalidKeySpecException | NoSuchAlgorithmException |XMPPException.XMPPErrorException |
               NotConnectedException | SmackException.NoResponseException e) {
            e.printStackTrace();
        }
        if(pub != null){
            String encrypted_message = bytes2Hex(encrypt(msg.getBody().getBytes(), pub));
            String msgID = msg.getPacketID();
            msg = new EnceladusMessage(msg.getTo(),msg.getType());
            msg.setPacketID(msgID);
            msg.setBody(encrypted_message);
        }

        try {
            DeliveryReceiptManager.addDeliveryReceiptRequest(msg);
            chat.sendMessage(msg);
            if (roster.getPresence(chat.getParticipant()).getType() == Presence.Type.unavailable) {
                msg.setSent(true);
            }
        } catch (NotConnectedException e) { }
        if (!msg.isSent()) {
        	Conversation.getUnsentMessages().add(msg);
        	//if (!resendWorkerThread.isAlive()) {
        		//resendWorkerThread = new Thread(resendWorker);
        		//resendWorkerThread.start();
        	//}
        }
        //if (Conversation.getUnsentMessages().isEmpty()) {
        	//resendWorker.getCountDownTimer().cancel();
       // }
        
    }

    @Override
    public void onCreate(){
    	super.onCreate();
    	Log.v("Service", "Service created");

        Contact.setResources(getResources());
    	
        /* Initializing the parallelism fields. */
        asmack = SmackAndroid.init(getApplicationContext());
        contacts = new ArrayList<Contact>();
        conversations = new ArrayList<Conversation>();
        threadpool = Executors.newCachedThreadPool();
        lock = new ReentrantLock(true);  
        
        resendWorker = new ResendWorker(this, getConversations());
        resendWorkerThread = new Thread(resendWorker);

    	Intent i = new Intent(this,DatabaseService.class);
    	bindService(i, databaseServiceConnection, Context.BIND_AUTO_CREATE);

        /* Set up the the TLS settings. */
        // TODO: Add our certificate to the client. This is optional, but it can improve the security.
        SSLSocketFactory factory = null;
        try {
            SSLContext context = SSLContext.getInstance("TLSv1.2");
            context.init(null, null ,null);
            factory = context.getSocketFactory();
        } catch (Exception e) {
            e.printStackTrace();
        }

        /* Initializing the configuration and connection */
        config = new ConnectionConfiguration(HOST, PORT, SERVICE);
        config.setSocketFactory(factory);
        config.setSecurityMode(ConnectionConfiguration.SecurityMode.enabled);
        config.setCompressionEnabled(true);
        /* This should activate the default Smack reconnection behavior.  */
        config.setReconnectionAllowed(true);

        connection = new XMPPTCPConnection(config);
        search = new UserSearchManager(connection);
    }

    @Override
    public IBinder onBind(Intent intent) {
        return mBinder;
    }

    public synchronized boolean isStarted() {
        return isStarted;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startID) {

        Log.v("Service", "On startCommand" );
        isStarted = true;

        Notification n = new Notification.Builder(getApplicationContext())
                .setContentTitle("Session is running")
                .setSmallIcon(R.drawable.notification_small)
                .build();

        startForeground(1447, n);
        /* Try to find an account. */
        // check if Accountdata is local saved and login
        appData = getSharedPreferences(SHARED_PREF_FILENAME, MODE_PRIVATE);
        if(checkSharedPreferencesKeyPresence(SessionService.NUMBER_TAG) &&
                checkSharedPreferencesKeyPresence(SessionService.PASSWORD_TAG)) {
            userJID = appData.getString(SessionService.NUMBER_TAG, null);
            pwd = appData.getString(SessionService.PASSWORD_TAG, null);
            hasAccount = true;
            if(checkSharedPreferencesKeyPresence(SessionService.PUB_KEY_TAG) &&
                    checkSharedPreferencesKeyPresence(SessionService.PRIV_KEY_TAG)) {
                pubKey = appData.getString(SessionService.PUB_KEY_TAG, null);
                savedPrivateKey = appData.getString(SessionService.PRIV_KEY_TAG, null);
                hasKey = true;
            }

        }

        if (hasKey) {
            try {
                KeyFactory keyFactory = KeyFactory.getInstance("RSA");
                PrivateKey privatekey_gen =
                        keyFactory.generatePrivate(new PKCS8EncodedKeySpec(hex2Bytes(savedPrivateKey)));
                RSAPrivateKeySpec rsaPrivateKeySpec = keyFactory.getKeySpec(privatekey_gen, RSAPrivateKeySpec.class);

                privateKey = KeyFactory.getInstance("RSA").generatePrivate(rsaPrivateKeySpec);
            } catch (InvalidKeySpecException | NoSuchAlgorithmException e) {
                e.printStackTrace();
            }
        }


        if(isAuthenticated()) {
            Log.v("Service", "The service received a start command although its running!" +
                    "Something went wrong");
            return START_NOT_STICKY;
        }

        saveTimer = new Timer();
        saveTimer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                Intent i = new Intent(SessionService.this, DatabaseService.class);
                startService(i);
            }
        },300000,3600000);

        /* Connect and login */
        if(hasAccount) {
        	threadpool.execute(XMPPLogin);
            /* Initialize the ChatManager */
            chatManager = ChatManager.getInstanceFor(connection);
            chatManager.addChatListener(new ChatManagerListener() {

                @Override
                public void chatCreated(Chat chat, boolean createdLocally) {
                    if (!createdLocally) {
                        chat.addMessageListener(defaultMessageListener);
                    }
                }

            });
        }
        return START_STICKY;
    }

    @Override
    public boolean onUnbind(Intent intent) {
        chatActivity = null;
        return super.onUnbind(intent);
    }

    public void onTaskRemoved(Intent rootIntent) {
        Intent i = new Intent(SessionService.this, DatabaseService.class);
        startService(i);
        Log.v("Service", "Service IS DEAD");
    }

    @Override
    public void onDestroy(){
        threadpool.execute(new Runnable() {
            @Override
            public void run() {
                try { connection.disconnect(); }
                catch (SmackException.NotConnectedException e) { }
            }
        });
        threadpool.shutdown();
        saveTimer.cancel();
    	Intent i = new Intent(SessionService.this, DatabaseService.class);
    	startService(i);
        unbindService(databaseServiceConnection);
        stopForeground(true);
        Log.v("Service", "Service stopped!");
        asmack.onDestroy();
    }

    private Boolean checkSharedPreferencesKeyPresence(String key){
        if(appData.contains(key) && appData.getString(key, null) != null){
            return true;
        }
        return false;
    }

    public boolean userExists(String jID){

        Form searchForm;
        try {
            searchForm = search.getSearchForm("search."+connection.getServiceName());

            Form answerForm = searchForm.createAnswerForm();
            answerForm.setAnswer("Username", true);

            answerForm.setAnswer("search", jID);

            ReportedData data = search.getSearchResults(answerForm,"search."+connection.getServiceName());

            if(data.getRows() != null)
            {
                List<ReportedData.Row> ls = data.getRows();
                for(ReportedData.Row r : ls)
                {
                    List<String> list = r.getValues("jid");
                    if(list.size() == 1)
                    {
                        return true;
                    }

                }
            }
            return false;
        } catch (SmackException.NoResponseException | XMPPException.XMPPErrorException | NotConnectedException e) {
            return false;
        }
    }

    public XMPPTCPConnection getConnection() {
        return connection;
    }

    public ChatManager getChatManager(){
        return chatManager;
    }


    public ArrayList<Contact> getContacts() {
        return contacts;
    }

    public void setContacts(ArrayList<Contact> contacts) {
        this.contacts = contacts;
    }

    public ArrayList<Conversation> getConversations() {
        return conversations;
    }

    public Collection<RosterEntry> getRosterEntries(){
        return rosterEntries;
    }

    public void setPresence(boolean isAvailable) {
        presence = new Presence((isAvailable) ? Presence.Type.available : Presence.Type.unavailable);
        try {
            connection.sendPacket(presence);
        } catch (SmackException.NotConnectedException e) {
            Log.v("Smack", "Error");
        }
    }

    public boolean foundAccount() {
        return hasAccount;
    }

    public boolean isAuthenticated() {
        return connection.isAuthenticated();
    }

    public void clearNotifications() {
        indexToNotify = -1;
        countUnread = 0;
        NotificationManager manager = (NotificationManager)getSystemService(NOTIFICATION_SERVICE);
        manager.cancelAll();
    }

    public Chat createChat(String partnerJID ){
        return chatManager.createChat(partnerJID, defaultMessageListener);
    }
    
    public static String getUserJID() {
    	return userJID;
    }
    
    public class SessionBinder extends Binder{
        public SessionService getService() {
            return SessionService.this;
        }
        public void setChatActivity(ChatActivity activity){
            chatActivity = activity;
        }
    }

    private Runnable XMPPLogin = new Runnable() {

		@Override
		public void run() {
			try {
				connection.connect();
	            connection.login(userJID, pwd, "Enceladus");
                VCard vc = new VCard();
                vc.load(connection);
                vc.setField("PubKey", pubKey);
                vc.save(connection);
	            if(connection.isConnected())
	                Log.v("Connection", "Connected");
	            if(connection.isUsingTLS())
	                Log.v("Connection", "Secure established");
	            if(connection.isAuthenticated())
	                Log.v("Connection", "Login successful");
	        }
	        catch (SmackException | IOException | XMPPException e) {
                Log.v("Login", "Error while connecting to the Server" + e.getMessage());
            }

            pingManager = PingManager.getInstanceFor(connection);
            pingManager.setPingInterval(15);

            deliveryReceiptManager = DeliveryReceiptManager.getInstanceFor(connection);
            deliveryReceiptManager.setAutoReceiptsEnabled(true);
            deliveryReceiptManager.addReceiptReceivedListener(receiptReceivedListener);
			Contact.setLastActivityManager(LastActivityManager.getInstanceFor(connection));
	        /* Set presence to available and sent the packet. */
	        setPresence(true);
	
	        /* Get roster and roster entries from the server */
	        Roster.setDefaultSubscriptionMode(Roster.SubscriptionMode.accept_all);
	        roster = connection.getRoster();
	        rosterEntries = roster.getEntries();

//            for(RosterEntry r : rosterEntries){
//                contacts.add(new Contact(r.getUser(),null, r.getName()));
//                Log.v("Roster", r.getUser());
//            }

	        /* Add a listener to the roster, which updates the roster entries. */
	        roster.addRosterListener(new RosterListener(){
	
	            @Override
	            public void entriesAdded(Collection<String> arg0) {


//	                rosterEntries = roster.getEntries();
//                    contacts.clear();
//                    for(RosterEntry r : rosterEntries){
//
//                        contacts.add(new Contact(r.getUser(),null, r.getName()));
//                    }
//                    if(chatActivity != null){
//                        chatActivity.notifyConversationChanged();
//                    }
	            }
	
	            @Override
	            public void entriesDeleted(Collection<String> arg0) {

//	                rosterEntries = roster.getEntries();
//                    for(String s : arg0){
//                       for(RosterEntry r : rosterEntries) {
//                           for (Contact c : contacts) {
//                               if (c.getjID().equals(s)) {
//                                   contacts.remove(c);
//
//                               }
//                           }
//                       }
                    //}

//                    if(chatActivity != null){
//                        chatActivity.notifyConversationChanged();
//                    }

	            }
	
	            @Override
	            public void entriesUpdated(Collection<String> arg0) {
                   // try {
                   //     connection.disconnect();
                   //     connection.connect();
                   //     connection.login(userJID,pwd, "Enceladus");
                   // } catch (XMPPException | SmackException | IOException e) {
                   //     e.printStackTrace();
                   // }
                }
	
	            @Override
	            public void presenceChanged(Presence arg0) {
	            }
	
	
	        });

            /* Create a chat for each Conversation */
            for(Conversation c : conversations) {
                c.setChat(getChatManager().createChat(c.getPartner().getjID(), defaultMessageListener));
            }
		}
    	
    };

	public Roster getRoster() {
		return roster;
	}
}
