package org.burntbunch.enceladus.controller;

import android.os.CountDownTimer;
import android.util.Log;

import org.burntbunch.enceladus.model.Conversation;
import org.jivesoftware.smack.Chat;

import java.util.ArrayList;


/**
 * Copyright 2014 BurntBunch-Team (www.burntbunch.org)

 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at

 http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 */

/**
 * ResendWorker to handle the Resend Behavior on flags in the ArrayLists from Conversations
 * Needs to get a reference on the SessionService and on the conversationList
 */

//TODO: Implement in SessionService

public class ResendWorker implements Runnable {

    private ArrayList<Conversation> conversations;
    private SessionService sessionService;
    private CountDownTimer countDownTimer;

    public ResendWorker(SessionService sessionService, ArrayList<Conversation> conversations){
        this.conversations = conversations;
        this.sessionService = sessionService;
        this.countDownTimer = new CountDownTimer(30000, 1000) {
			
        	public void onTick(long millisUntilFinished) {
                //TODO: check if ThreadPool in the SessionService is still alive
        		Log.v("Timer", "Tick");
                for (int i=0; i<Conversation.getUnsentMessages().size();i++){
                    if(Conversation.getUnsentMessages().get(i).isReceived()){
                        Conversation.getUnsentMessages().remove(i);
                        i--;
                    }
                }
            }

            public void onFinish() {
                for (int i=0; i<Conversation.getUnsentMessages().size();i++){
                    ResendWorker.this.sessionService.engage(Conversation.getUnsentMessages().get(i),
                    										findChatByjID(Conversation.getUnsentMessages().get(i).getTo()));
                }
            }
		};
    }

    public Chat findChatByjID(String jID){
        for(Conversation c : conversations){
            if(jID.equals(c.getPartner().getjID()) ){
                return c.getChat();
            }
        }
        return null;
    }
    
	public CountDownTimer getCountDownTimer() {
		return countDownTimer;
	}

    @Override
    public void run() {
            countDownTimer.start();
    }
}
