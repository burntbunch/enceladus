package org.burntbunch.enceladus.ui;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.app.Activity;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.burntbunch.enceladus.R;
import org.burntbunch.enceladus.controller.SessionService;
import org.jivesoftware.smack.AccountManager;
import org.jivesoftware.smack.ConnectionConfiguration;
import org.jivesoftware.smack.SmackException.NoResponseException;
import org.jivesoftware.smack.SmackException.NotConnectedException;
import org.jivesoftware.smack.XMPPException.XMPPErrorException;
import org.jivesoftware.smack.tcp.XMPPTCPConnection;

import java.security.KeyPair;
import java.security.KeyPairGenerator;

import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocketFactory;

/**
 * Copyright 2014 BurntBunch-Team (www.burntbunch.org)

 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at

 http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 */

/**
 * Activity which displays a login screen to the user, offering registration as
 * well.
 */
public class RegisterActivity extends Activity {

	/**
	 * Keep track of the login task to ensure we can cancel it if requested.
	 */
	private UserLoginTask mAuthTask = null;

	// Values for email and password at the time of the login attempt.
	private String mNumber;
	private String mPassword;
    private String mPrivKey;
    private String mPubKey;

	// UI references.
	private EditText mNumberView;
	private EditText mPasswordView;
    private EditText mReenterPasswordView;
	private View mLoginFormView;
	private View mLoginStatusView;
	private TextView mLoginStatusMessageView;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.register_activity);

        // Set up the login form.
		mNumberView = (EditText) findViewById(R.id.number);
		mNumberView.setText(mNumber);

		mPasswordView = (EditText) findViewById(R.id.password);

        mReenterPasswordView = (EditText) findViewById(R.id.reenterPassword);
        mReenterPasswordView.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int id,
                                          KeyEvent keyEvent) {
                if (id == R.id.login || id == EditorInfo.IME_NULL) {
                    attemptLogin();
                    return true;
                }
                return false;
            }
        });

		mLoginFormView = findViewById(R.id.login_form);
		mLoginStatusView = findViewById(R.id.login_status);
		mLoginStatusMessageView = (TextView) findViewById(R.id.login_status_message);

		findViewById(R.id.sign_in_button).setOnClickListener(
				new View.OnClickListener() {
					@Override
					public void onClick(View view) {
						attemptLogin();
					}
				});
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		super.onCreateOptionsMenu(menu);
		getMenuInflater().inflate(R.menu.register, menu);
		return true;
	}

	/**
	 * Attempts to sign in or register the account specified by the login form.
	 * If there are form errors (invalid email, missing fields, etc.), the
	 * errors are presented and no actual login attempt is made.
	 */
	public void attemptLogin() {
		if (mAuthTask != null) {
			return;
		}

		// Reset errors.
		mNumberView.setError(null);
		mPasswordView.setError(null);

		// Store values at the time of the login attempt.
		mNumber = mNumberView.getText().toString();
		mPassword = mPasswordView.getText().toString();

		boolean cancel = false;
		View focusView = null;

		// Check for a valid password.
		if (TextUtils.isEmpty(mPassword)) {
			mPasswordView.setError(getString(R.string.error_field_required));
			focusView = mPasswordView;
			cancel = true;
		} else if (mPassword.length() < 4) {
			mPasswordView.setError(getString(R.string.error_invalid_password));
			focusView = mPasswordView;
			cancel = true;
		} else if(!mPasswordView.getText().toString().equals(mReenterPasswordView.getText().toString())){
            mReenterPasswordView.setError(getString(R.string.error_password_doesnt_match));
            focusView = mReenterPasswordView;
            cancel = true;
        }

		// Check for a valid email address.
		if (TextUtils.isEmpty(mNumber)) {
			mNumberView.setError(getString(R.string.error_field_required));
			focusView = mNumberView;
			cancel = true;
		}

		if (cancel) {
			// There was an error; don't attempt login and focus the first
			// form field with an error.
			focusView.requestFocus();
		} else {
			// Show a progress spinner, and kick off a background task to
			// perform the user login attempt.
			mLoginStatusMessageView.setText(R.string.login_progress_signing_in);
			InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
			showProgress(true);
			mAuthTask = new UserLoginTask();
			mAuthTask.execute((Void) null);
		}
	}

	/**
	 * Shows the progress UI and hides the login form.
	 */
	private void showProgress(final boolean show) {
        int shortAnimTime = getResources().getInteger(
                android.R.integer.config_shortAnimTime);

        mLoginStatusView.setVisibility(View.VISIBLE);
        mLoginStatusView.animate().setDuration(shortAnimTime)
                .alpha(show ? 1 : 0)
                .setListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        mLoginStatusView.setVisibility(show ? View.VISIBLE
                                : View.GONE);
                    }
                });

        mLoginFormView.setVisibility(View.VISIBLE);
        mLoginFormView.animate().setDuration(shortAnimTime)
                .alpha(show ? 0 : 1)
                .setListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        mLoginFormView.setVisibility(show ? View.GONE
                                : View.VISIBLE);
                    }
                });
	}

    public static String bytes2Hex(byte[] input) {

         String output = "";
         if(input.length == 0)
                return output;
         String s;
         for(int i = 0; i < input.length; i++) {
            s = String.format("%x", input[i]);
         if(s.length() == 1)
            s = "0" + s;
         output += s;
        }
        return output;
    }

	/**
	 * Represents an asynchronous login/registration task used to authenticate
	 * the user.
	 */
	public class UserLoginTask extends AsyncTask<Void, Void, Boolean> {
		@Override
		protected Boolean doInBackground(Void... params) {
			

			//TODO: Delete login stuff, only use this activity to register (and maybe to login afterwards)
            ConnectionConfiguration config = new ConnectionConfiguration(SessionService.HOST, SessionService.PORT, SessionService.SERVICE);
            SSLSocketFactory factory = null;
            try {
                SSLContext context = SSLContext.getInstance("TLSv1.2");
                context.init(null, null ,null);
                factory = context.getSocketFactory();
            } catch (Exception e) {
                e.printStackTrace();
            }

        /* Initializing the configuration and connection */
            config.setSocketFactory(factory);
            config.setSecurityMode(ConnectionConfiguration.SecurityMode.enabled);
            config.setCompressionEnabled(true);
            XMPPTCPConnection connection = new XMPPTCPConnection(config);

			AccountManager accMan = AccountManager.getInstance(connection);
            try {
                connection.connect();
                accMan.createAccount(mNumber, mPassword);
                runOnUiThread(new Runnable() {
					
					@Override
					public void run() {
						mLoginStatusMessageView.setText(R.string.login_progress_generating_keys);
					}
				});
                KeyPairGenerator kpg = KeyPairGenerator.getInstance("RSA");
                kpg.initialize(2048);
                KeyPair keyPair = kpg.generateKeyPair();
                mPrivKey = bytes2Hex(keyPair.getPrivate().getEncoded());
                mPubKey = bytes2Hex(keyPair.getPublic().getEncoded());
                return true;
            }
            catch (XMPPErrorException e) { Log.e("XMPP", e.getMessage()); }
            catch (NoResponseException e ) {
                Toast.makeText(getApplicationContext(), getString(R.string.error_message_no_connection), Toast.LENGTH_LONG).show();
            }
            catch (Exception e) { e.printStackTrace(); }

			return false;
			}

		@Override
		protected void onPostExecute(final Boolean success) {
			mAuthTask = null;
			showProgress(false);

			if (success) {

                // fake authentication
                sendFakeSMS();
//                if(readFakeSMS()) {
//                    Toast.makeText(getBaseContext(), "Authentifizierung erfolgt!", Toast.LENGTH_SHORT);
//                }

                //Save Accountdata
                SharedPreferences.Editor editor = getSharedPreferences("Enceladus", MODE_PRIVATE).edit();
                editor.putString(SessionService.NUMBER_TAG, mNumber);
                editor.putString(SessionService.PASSWORD_TAG, mPassword);
                editor.putString(SessionService.PRIV_KEY_TAG, mPrivKey);
                editor.putString(SessionService.PUB_KEY_TAG, mPubKey);
                editor.commit();
                startService(new Intent(RegisterActivity.this, SessionService.class));
                finish();
			} else {
				Log.v("Register", "Something went wrong!");
				mPasswordView.requestFocus();
			}
		}

		@Override
		protected void onCancelled() {
			mAuthTask = null;
			showProgress(false);
		}

        protected boolean sendFakeSMS(){

            try {
                ContentValues contentValues = new ContentValues();
                contentValues.put("address", "+99887766");
                contentValues.put("body", "Congratulation! Your Enceladus Account is registered.");
                contentValues.put("read", 1);
                contentValues.put("status", -1);
                contentValues.put("type", 2);
                //contentValues.put("date", )
                getContentResolver().insert(Uri.parse("content://sms/inbox"), contentValues);
            }catch(Exception ex) {
                return false;
            }
            return true;
        }


        protected Boolean readFakeSMS(){

            Cursor c = getContentResolver().query(
                    Uri.parse("content://sms/inbox"),
                    new String[]{"_id","adress","person"}, //etc
                    null,
                    null,
                    null
            );

            if( c != null ) {
                c.moveToLast();
                if(c.getCount() > 0) {
                    do {
                        String sender = c.getString(c.getColumnIndex("adress"));
                        if(sender.equals("+99887766")) {
                            return true;
                        }
                    } while(c.moveToPrevious());
                }
            }

            return false;

        /*  0                      :      _id
            1                      :     thread_id
            2                      :     address
            3                      :     person
            4                      :     date
            5                      :     protocol
            6                      :     read
            7                      :    status
            8                      :    type
            9                      :    reply_path_present
            10                   :    subject
            11                   :    body
            12                   :    service_center
            13                   :    locked
        */
        }
	}
}
