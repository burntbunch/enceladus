package org.burntbunch.enceladus.ui.fragment;

import java.util.ArrayList;

import org.burntbunch.enceladus.ui.ChatActivity;
import org.burntbunch.enceladus.R;
import org.jivesoftware.smack.Chat;

import android.app.Activity;
import android.app.ListFragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

/**
 * Copyright 2014 BurntBunch-Team (www.burntbunch.org)

 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at

 http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 */

public class UserListFragment extends ListFragment {
	
	ArrayList<Chat> values;
    private onConversationSelectedListener callback;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
			super.onActivityCreated(savedInstanceState);

	}

    public interface onConversationSelectedListener {
        public void onConversationSelected(int position);
    }

    @Override
    public void onAttach(Activity activity) {
        callback = (ChatActivity)activity;
        super.onAttach(activity);
    }



    @Override
	public void onListItemClick(android.widget.ListView l, View v, int position, long id) {
        callback.onConversationSelected(position);
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		return inflater.inflate(R.layout.user_list_fragment, container, true);
	}
}
