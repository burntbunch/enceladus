package org.burntbunch.enceladus.ui;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import org.burntbunch.enceladus.controller.SessionService;
import org.burntbunch.enceladus.model.ChatAdapter;
import org.burntbunch.enceladus.model.Contact;
import org.burntbunch.enceladus.model.Conversation;
import org.burntbunch.enceladus.model.UserListAdapter;
import org.burntbunch.enceladus.ui.fragment.UserListFragment;
import org.burntbunch.enceladus.R;
import org.jivesoftware.smack.SmackException.NoResponseException;
import org.jivesoftware.smack.SmackException.NotConnectedException;
import org.jivesoftware.smack.XMPPException.XMPPErrorException;

import android.app.ActionBar;
import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.os.IBinder;
import android.support.v4.widget.SlidingPaneLayout;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ListView;

/**
 * Copyright 2014 BurntBunch-Team (www.burntbunch.org)

 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at

 http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 */

public class ChatActivity extends Activity implements UserListFragment.onConversationSelectedListener {

	private ListView listView;
	private UserListFragment userList;
	private ChatAdapter adapter;
	private UserListAdapter userListAdapter;
	private SlidingPaneLayout slidingPane;
    private ActionBar actionBar;
    private ArrayList<Conversation> conversations;
    private final int NO_CONVERSATION = -1;
    private int activeConversation = NO_CONVERSATION;
    private SharedPreferences appData;
    private boolean hasSavedInstanceState;
    private boolean isInBackground;
    public static final String ACTIVE_CONVERSATION = "ChatActivity.ACTIVE_CONVERSATION";

    private ServiceConnection sessionServiceConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            sessionService = ((SessionService.SessionBinder)service).getService();
            ((SessionService.SessionBinder)service).setChatActivity(ChatActivity.this);
            conversations = sessionService.getConversations();
            if(activeConversation != NO_CONVERSATION){
                adapter.reloadConversation(conversations.get(activeConversation).getMessages());
            }
            listView.setAdapter(adapter);
            notifyConversationChanged();
            Bundle extras = getIntent().getExtras();
            if(extras != null && !hasSavedInstanceState) {
                onConversationSelected(extras.getInt(ACTIVE_CONVERSATION));
            }
            /* Check if the service is not persistent started. */
            if (!sessionService.isStarted()) {
                startService(sessionIntent);
            }
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            sessionService = null;
        }
    };
    private SessionService sessionService;
    private Intent sessionIntent;


	@Override
	protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.chat_activity);

        if(savedInstanceState != null){
            activeConversation = savedInstanceState.getInt(ACTIVE_CONVERSATION);
            hasSavedInstanceState = true;
        }

        adapter = new ChatAdapter(this);
        sessionIntent = new Intent(this, SessionService.class);
        bindService(sessionIntent, sessionServiceConnection, Context.BIND_AUTO_CREATE);
        slidingPane = (SlidingPaneLayout) findViewById(R.id.slidingpane);
        actionBar = getActionBar();
        userList = (UserListFragment) getFragmentManager().findFragmentById(R.id.leftpane);
        listView = (ListView) findViewById(R.id.chatlist);
        slidingPane.setPanelSlideListener(new SlidingPaneLayout.PanelSlideListener() {
            @Override
            public void onPanelSlide(View view, float v) {
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
            }

            @Override
            public void onPanelOpened(View view) {
                actionBar.setTitle(R.string.app_name);
                actionBar.setDisplayHomeAsUpEnabled(false);
                actionBar.setLogo(R.drawable.logo_flat);
                actionBar.setSubtitle(null);
            }

            @Override
            public void onPanelClosed(View view) {
                if (activeConversation != NO_CONVERSATION) {
                    Contact currentPartner = conversations.get(activeConversation).getPartner();
                    if (currentPartner.getAvatar() != null) {
                        actionBar.setLogo(new BitmapDrawable(getResources(), currentPartner.getAvatar()));
                    } else {
                        actionBar.setLogo(R.drawable.logo_flat);
                    }
                    actionBar.setTitle(currentPartner.getName());
                    try {
                    	Date last = currentPartner.getLastSeen();
                        actionBar.setSubtitle(getText(R.string.last_seen) + " "
                        					  + SimpleDateFormat.getDateTimeInstance().format(last));
                    } catch (NoResponseException | XMPPErrorException | NotConnectedException e) {
                        actionBar.setSubtitle(null);
                    }
                }
                actionBar.setDisplayHomeAsUpEnabled(true);
            }
        });

        if (savedInstanceState == null) {
            Contact.setResources(getResources());
            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
            slidingPane.openPane();

        }

        appData = getSharedPreferences(SessionService.SHARED_PREF_FILENAME, MODE_PRIVATE);
        if(checkSharedPreferencesKeyPresence(SessionService.NUMBER_TAG) &&
                checkSharedPreferencesKeyPresence(SessionService.PASSWORD_TAG)) {
            String userJID = appData.getString(SessionService.NUMBER_TAG, null);
            String  pwd = appData.getString(SessionService.PASSWORD_TAG, null);
            if(userJID == null || pwd == null) {
                Intent i = new Intent(this, RegisterActivity.class);
                startActivity(i);
            }
        }
        else {
            Intent i = new Intent(this, RegisterActivity.class);
            startActivity(i);
        }
    }

    @Override
    protected void onNewIntent(Intent intent) {

        Bundle extras = intent.getExtras();
        if(extras != null) {
            onConversationSelected(extras.getInt(ACTIVE_CONVERSATION));
        }
    }

    @Override
    protected void onPause() {
        isInBackground = true;
        super.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if(sessionService != null) {
            //sessionService.clearNotifications();
        }
        isInBackground = false;
    }

    @Override
    public void onConversationSelected(int position) {
        activeConversation = position;
        adapter.reloadConversation(conversations.get(activeConversation).getMessages());
        slidingPane.closePane();
    }

    public void notifyConversationChanged() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                userListAdapter = new UserListAdapter(ChatActivity.this, conversations);
                userList.setListAdapter(userListAdapter);
                adapter.notifyDataSetChanged();
                userListAdapter.notifyDataSetChanged();
            }
        });
    }

    @Override
    public void onBackPressed() {
        if(!slidingPane.isOpen()) {
            slidingPane.openPane();
        }
        else{
            super.onBackPressed();
        }
    }

    public void sendMessage(View view) {
        if( (activeConversation != NO_CONVERSATION) && (conversations.get(activeConversation).getChat() != null) ) {
            String msg = ((EditText) findViewById(R.id.editText1)).getText().toString();
            sessionService.sendMessage(conversations.get(activeConversation), msg);
            adapter.notifyDataSetChanged();
        }
        ((EditText) findViewById(R.id.editText1)).setText("");
	}

    public int findConversationByjID(String jID){
        for(Conversation c : conversations){
            if(jID.contains(c.getPartner().getjID()) ){
                return conversations.indexOf(c);
            }
        }
        return NO_CONVERSATION;
    }

    public int createNewConversation(Contact contact){
        Conversation con = new Conversation();
        con.setChat(sessionService.createChat(contact.getjID()));
        con.setPartner(contact);
        conversations.add(con);
        userListAdapter.notifyDataSetChanged();
        return conversations.size()-1;
    }

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {

        MenuItem item = menu.findItem(R.id.contact_list);
        item.setVisible(true);
        return super.onPrepareOptionsMenu(menu);
    }

    @Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
            case R.id.action_settings:
                return true;
            case R.id.contact_list:
                showContactList();
            case android.R.id.home:
                if(!slidingPane.isOpen())
                    onBackPressed();
                break;
        }
		return super.onOptionsItemSelected(item);
	}

    public void showContactList(){
        Intent intent = new Intent(ChatActivity.this, ContactListActivity.class);
        startActivityForResult(intent, 0);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data){
        if(resultCode == RESULT_OK){

            int pos = data.getExtras().getInt("Position");
            Contact contact = sessionService.getContacts().get(pos);

            activeConversation = findConversationByjID(contact.getjID());
            if(activeConversation == NO_CONVERSATION) {
                createNewConversation(contact);
                activeConversation = findConversationByjID(contact.getjID());
            }
            userListAdapter.notifyDataSetChanged();
            onConversationSelected(activeConversation);
        }
        for (Contact contact : sessionService.getContacts()) {
            for (Conversation conversation : sessionService.getConversations()) {
                if (conversation.getPartner().getjID().equals(contact.getjID())) {
                    conversation.setPartner(contact);
                }
            }
        }
        notifyConversationChanged();
    }

    private Boolean checkSharedPreferencesKeyPresence(String key){
        if(appData.contains(key) && appData.getString(key, null) != null){
            return true;
        }
        return false;
    }


    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putInt(ACTIVE_CONVERSATION,activeConversation);
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onDestroy(){
    	unbindService(sessionServiceConnection);
        //TODO: Save all Conversations in Database
        super.onDestroy();
    }

    public boolean isInBackground() {
        return isInBackground;
    }
}
