package org.burntbunch.enceladus.ui;

import android.app.ListActivity;
import android.app.LoaderManager;
import android.app.ProgressDialog;
import android.content.ComponentName;
import android.content.CursorLoader;
import android.content.Intent;
import android.content.Loader;
import android.content.ServiceConnection;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.IBinder;
import android.provider.ContactsContract;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;

import org.burntbunch.enceladus.controller.SessionService;
import org.burntbunch.enceladus.model.Contact;
import org.burntbunch.enceladus.model.ContactAdapter;
import org.burntbunch.enceladus.R;
import org.jivesoftware.smack.SmackException.NoResponseException;
import org.jivesoftware.smack.SmackException.NotConnectedException;
import org.jivesoftware.smack.SmackException.NotLoggedInException;
import org.jivesoftware.smack.XMPPException.XMPPErrorException;
import org.jivesoftware.smackx.iqlast.LastActivityManager;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

/**
 * Copyright 2014 BurntBunch-Team (www.burntbunch.org)

 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at

 http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 */

public class ContactListActivity extends ListActivity implements LoaderManager.LoaderCallbacks<Cursor>{

    private ArrayList<Contact> mContacts;
    private ContactAdapter mAdapter;
    private LastActivityManager la;
    private SearchContactList s = new SearchContactList();
    private ProgressDialog progressDialog;
    private ServiceConnection sessionServiceConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            sessionService = ((SessionService.SessionBinder)service).getService();
            mContacts = sessionService.getContacts();
            //mAdapter.setContacts(mContacts);
            mAdapter = new ContactAdapter(ContactListActivity.this,R.layout.contact_list_row, mContacts);
            setListAdapter(mAdapter);
            mAdapter.notifyDataSetChanged();
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
        	sessionService = null;
        }
    };
    private SessionService sessionService;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.contact_list_activity);

        mContacts = new ArrayList<Contact>();
        mAdapter = new ContactAdapter(this, R.layout.contact_list_row, mContacts);
        setListAdapter(mAdapter);
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Loading Contacts...");
        progressDialog.show();
        Intent sessionIntent = new Intent(this, SessionService.class);
        bindService(sessionIntent, sessionServiceConnection, BIND_AUTO_CREATE);

        if (savedInstanceState == null) {
            getLoaderManager().initLoader(0, null, this);
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {

        MenuItem item = menu.findItem(R.id.contact_list);
        item.setVisible(false);
        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    protected void onListItemClick(ListView listview, View v, int position, long id){

        Intent intent = new Intent();

        intent.putExtra("Position", position);
        setResult(RESULT_OK, intent);
        finish();
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                    onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        finish();
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle bundle) {

        final String[] PROJECTION = new String[]{
                ContactsContract.CommonDataKinds.Phone._ID,
                ContactsContract.CommonDataKinds.Phone.TYPE,
                ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME,
                ContactsContract.CommonDataKinds.Phone.NORMALIZED_NUMBER,
                ContactsContract.CommonDataKinds.Photo.PHOTO_ID
        };

        final String mSelection = "((" + ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME + " NOTNULL) AND (" + ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME + " != '' ) AND ("+
                ContactsContract.CommonDataKinds.Phone.NUMBER + " NOTNULL))";

        return new CursorLoader(
                this,
                ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
                PROJECTION,
                mSelection,
                null,
                ContactsContract.Data.DISPLAY_NAME_PRIMARY
        );
    }

    @Override
    public void onLoadFinished(Loader<Cursor> cursorLoader, Cursor cursor) {
     s.execute(cursor);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> cursorLoader) {

    }

    private Bitmap queryContactImage(int imageDataRow) {
        Cursor c = getContentResolver().query(ContactsContract.Data.CONTENT_URI, new String[] {
                ContactsContract.CommonDataKinds.Photo.PHOTO
        }, ContactsContract.Data._ID + "=?", new String[] {
                Integer.toString(imageDataRow)
        }, null);
        byte[] imageBytes = null;
        if (c != null) {
            if (c.moveToFirst()) {
                imageBytes = c.getBlob(0);
            }
            c.close();
        }

        if (imageBytes != null) {
            return BitmapFactory.decodeByteArray(imageBytes, 0, imageBytes.length);
        } else {
            return null;
        }
    }

    @Override
    protected void onDestroy() {
        unbindService(sessionServiceConnection);
        s.cancel(true);
        super.onDestroy();
    }

    private void syncContacts(Boolean override, ArrayList<Contact> changedList){
        for(Contact c : changedList){
            Boolean isFound = false;
            for( Contact mc : mContacts){
                if(mc.getjID().equals(c.getjID())){
                    isFound = true;
                    if(override){
                        mc.setName(c.getName());
                        mc.setAvatar(c.getAvatar());
                    }
                }
            }
            if(!isFound)
                mContacts.add(c);
        }
        Collections.sort(mContacts, new Comparator<Contact>() {
            @Override
            public int compare(Contact contact, Contact contact2) {
                return contact.getName().compareTo(contact2.getName());
            }
        });
    }

    private class SearchContactList extends AsyncTask<Cursor,Void,ArrayList<Contact>>{



        @Override
        protected ArrayList<Contact> doInBackground(Cursor... params) {
            ArrayList<Contact> aContacts = new ArrayList<Contact>();
            Cursor cursor = params[0];
            cursor.moveToFirst();
            while(!cursor.isAfterLast()) {
                Log.v("WhileSearch","Searching");
                //int id = cursor.getInt(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone._ID));
                //int type = cursor.getInt(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.TYPE));
                String displayName = cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME));
                String number = cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NORMALIZED_NUMBER));
                int photoId = cursor.getInt(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Photo.PHOTO_ID));
                if(number != null && sessionService != null && sessionService.userExists(number)) {
                    Contact c = new Contact(number + "@burntbunch.org", queryContactImage(photoId), displayName);
                    aContacts.add(c);
                    Log.v("WhileSearch", c.getjID());
                	try {
						sessionService.getRoster().createEntry(number + "@burntbunch.org", displayName, null);
					} catch (NotLoggedInException | NoResponseException
							| XMPPErrorException | NotConnectedException e) {
                        Log.v("WhileSearch", "Exception creating Roster Entry" + e.getMessage());
					}
                }
                cursor.moveToNext();
            }
            cursor.close();
            return aContacts;
        }

        /*protected PublicKey getKeyFromVcard (Contact contact){
            PublicKey pub = null;
            try {
                VCard vc = new VCard();
                //Log.v("Message", msg.getBody());
                vc.load(sessionService.getConnection(), contact.getjID());
                String pubKey = vc.getField("PubKey");
                KeyFactory keyFactory = KeyFactory.getInstance("RSA");
                PublicKey publicKey =
                        keyFactory.generatePublic(new X509EncodedKeySpec(hex2Bytes(pubKey)));
                RSAPublicKeySpec rsaPublicKeySpec = keyFactory.getKeySpec(publicKey, RSAPublicKeySpec.class);

                pub = KeyFactory.getInstance("RSA").generatePublic(rsaPublicKeySpec);

                return pub;
            } catch (InvalidKeySpecException | NoSuchAlgorithmException |XMPPException.XMPPErrorException |
                    NotConnectedException | SmackException.NoResponseException e) {
                e.printStackTrace();
            }
            return pub;
        }

        public byte[] hex2Bytes(String s) {
            int len = s.length();
            byte[] data = new byte[len / 2];
            for (int i = 0; i < len; i += 2) {
                data[i / 2] = (byte) ((Character.digit(s.charAt(i), 16) << 4)
                        + Character.digit(s.charAt(i+1), 16));
            }
            return data;
        }*/

        @Override
        protected void onPostExecute(ArrayList<Contact> contacts) {
            Log.v("WhileSearch", "PostExecute");
            //mContacts.clear();
            //mContacts.addAll(contacts);
            //mContacts.add(new Contact("lerentis@burntbunch.org"));
            syncContacts(true, contacts);
            mAdapter.notifyDataSetChanged();
            progressDialog.dismiss();
            super.onPostExecute(contacts);
        }
    }

}
