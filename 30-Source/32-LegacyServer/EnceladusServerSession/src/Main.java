
import java.io.IOException;
import java.io.InterruptedIOException;
import java.net.*;
import java.util.ArrayList;

public class Main {

	// Liste f�r die aufgebauten Sessions
	private static ArrayList<Session> clientList = new ArrayList<Session>();
	// Maximal zugelassenen Sessions
	private final static int MAX_SOCKETS = 6;
	
	//Defines f�r die StateMachine
	private static final int STATE_INIT = 0;
	private static final int STATE_LISTEN = 1;
	private static final int STATE_RUNNING = 2;
	private static final int STATE_STOP = 3;
	private static final int STATE_ERROR = 4;

	// Aktueller Status der Statemachine  
	private static int state = STATE_INIT;
	
	private static ServerSocket sSocket;
	
	public static void main(String[] args) {

		// Neuer Thread f�r die StateMachine
		new Thread(new Runnable(){
			int port = 9999;
				
			public void run(){
				
					// State_Machine
					while(state != STATE_STOP){
						switch(state){
							// Initialisierung
							case STATE_INIT:
									// Erstellen des ServerSockets und eines 60s TimeoutTimers
									try{
										System.out.println("[Server]: Server wird gestartet");
										sSocket = new ServerSocket(port);
										sSocket.setSoTimeout(60000);
										state++;
									}catch(IOException ex)
									{
										System.out.println("[Server-Error]: Fehler beim erstellen des SocketServers!");
										state=STATE_ERROR;
									}
								break;
							// Maximale Anzahl Clienten nicht erreicht, daher (weiter-)horchen
							case STATE_LISTEN:
									try{
										if(clientList.size() <  MAX_SOCKETS){
											System.out.println("[Server]: Warte auf Clienten...");
											clientList.add(new Session(sSocket.accept()));
											System.out.println("[Server]: Client verbunden! Anzahl Sockets: "+clientList.size());
											new Thread(clientList.get(clientList.size()-1)).start();
											
											if(clientList.size() % 2 == 0){
												clientList.get(clientList.size()-1).setPartner(clientList.get(clientList.size()-2));
												clientList.get(clientList.size()-2).setPartner(clientList.get(clientList.size()-1));
											}
										}
										else{
											//sSocket.close();
											System.out.println("[Server]: Anzahl MaxClienten erreicht!");
											//clientList.get(0).setPartner(clientList.get(1));
											//clientList.get(1).setPartner(clientList.get(0));											
											state++;
										}
									}
									catch (InterruptedIOException ex2)
									{ 
										// Meldungen nach ablauf des TimeoutTimers
										if(clientList.isEmpty())
											System.out.println("[Server-Message]: TimeOut! Kein Client gefunden");
										else{
											System.out.println("[Server-Message]: �berpr�fe Client Connection...");
											checkClientConnection();
											System.out.println("[Server-Message]: Aktive Clienten - "+clientList.size());
										}
									}
									catch(IOException ex1)
									{ System.out.println(ex1.getMessage());	}
								break;
							// Maxanzahl Clienten erreicht, keine weitere Aufnahme von Clients m�glich
							case STATE_RUNNING:
									// �berpr�fen ob eine Session beendet wurde und ggf. diese aus der aktiven Liste entfernen
									checkClientConnection();
									
									// Falls weitere Clienten aufgenommen werden k�nnen dann State wieder auf "Horchen" setzen 
									if(clientList.size() <  MAX_SOCKETS)
										state = STATE_LISTEN;
								break;
							// Fehlerausgabe und Restart des SocketServers
							case STATE_ERROR: 
									System.out.println("[Server-Error]:Fehler augfetreten. Starte Serversocket neu...");									
							default: 
									state = STATE_INIT;
								break;
						}
					}
			}
		}).start();	
		
		// Aufrechthaltung des MainThreads
		while(true);
	}
	
	/**
	 * Externe M�glichkeit die StateMachine anzusteuern
	 * @param newState Neuer Status f�r die StateMachine
	 */
	public static void setState(int newState){
		state = newState;
	}
	
	public static void checkClientConnection(){
		for(Session s : clientList.toArray(new Session[0]))
		{	
			if(s.checkState() == false){
				clientList.remove(s);
				s.stopSession();
			}
		}	
	}
}
