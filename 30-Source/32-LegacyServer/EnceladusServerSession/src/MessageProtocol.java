

public class MessageProtocol {

	private String message, source, destination;
	
	public MessageProtocol(String message, String source, String destination) {
		this.message = message;
		this.source = source;
		this.destination = destination;
	}

	public static MessageProtocol getInstance(String message, String source, String destination) {
		//Check stuff
		return new MessageProtocol(message, source, destination);
	}

	public String getMessage() {
		return message;
	}

	public String getSource() {
		return source;
	}

	public String getDestination() {
		return destination;
	}
}
