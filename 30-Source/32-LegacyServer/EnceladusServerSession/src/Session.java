import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;

import com.google.gson.Gson;


public class Session implements Runnable{
	
	private Socket client;
	private boolean running;
	private boolean notIdle;
	
	private PrintWriter out;
	private BufferedReader in;
	
	private Session partner;
	
	private Gson gson;
	
	/**
	 * Konstruktor f�r eine Sessionverbindung
	 * @param client Socketverbindung zum Clienten
	 */
	public Session( Socket client){
		this.client = client;
	}

	@Override
	public void run() {
		System.out.println("[Session]Running...");
		try{
			out = new PrintWriter(new BufferedWriter(new OutputStreamWriter(client.getOutputStream())), true);
			in = new BufferedReader(new InputStreamReader(client.getInputStream()));
			
			running = true;
			
			while(running){
				String s = in.readLine();

				if(s != null){
					s = changeSenderReceiverIds(s);
					
					if(partner != null && partner.running)						
						partner.sendMessage(s);
					else
						sendMessage(s);
					
					notIdle = true;
				}
			}
		}catch(Exception ex)
		{ System.out.println("[Session-Error]Fehler"); }

		running = false;
		System.out.println("[Session]Session wird beendet...");
	}
	
	/**
	 * Abfrage des Session Threads
	 * @return Zustand des Sessions(Boolean)
	 */
	public boolean checkState(){
		if(notIdle){
			notIdle = false;
			return true;
		}
		
		return false;
	}
	
	/**
	 * Durch aufrufen dieser Methode wird die Session gestoppt
	 */
	public void stopSession(){
		running = false;
		partner.setPartner(null);
	}
	
	/**
	 * Nachricht an den Clienten versenden
	 * @param msg Stringobjekt welches �ber die Socketverdindung gesendet werden soll
	 */
	public void sendMessage(String msg){
		
		out.println(msg);
		out.flush();
		
		notIdle = true;
	}
	
	/**
	 * Serialisiert das Messagprotollobjekt mithilfe von Gson zu einem Strinobjekt
	 * @param protocol MessagProtokol Objekt welches serialisiert werden soll
	 * @return
	 */
	private String serialize(MessageProtocol protocol) {
		if (gson == null) {
			gson = new Gson();
		}
		return gson.toJson(protocol);
	}
	
	/**
	 * Deserialisert einen String in einen Objekt vom Typ MessageProtokoll
	 * @param json String welches deserialisert werden soll
	 * @return
	 */
	private MessageProtocol deserialize(String json) {
		if (gson == null) {
			gson = new Gson();
		}
		return (MessageProtocol) gson.fromJson(json, MessageProtocol.class);
	}
	
	/**
	 * Vertauscht den Absender mit dem Empf�nger
	 * @param msg Nachricht welches mithilfe der Klasse MessageProtokoll bearbeitet werden soll
	 * @return
	 */
	private String changeSenderReceiverIds(String msg){
		MessageProtocol echo;
		
		echo = deserialize(msg);
		System.out.println(echo.getSource());
		System.out.println(echo.getDestination() );
		System.out.println(echo.getMessage());
		
		echo = new MessageProtocol(echo.getMessage(), echo.getDestination(), echo.getSource());
		return serialize(echo);
	}
	
	/**
	 * Weist der Session einen Kommunikationspartner zu
	 * @param partner Partner vom Typ Session
	 */
	public void setPartner( Session partner){
		this.partner = partner;
	}

}
